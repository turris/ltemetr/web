# ![NetMetr](https://www.netmetr.cz/theme/images/netmetr-logo.svg) `ltemetr-web`

Web interface for controlling continuous LTE speed/quality measurements running on multiple devices.

![screenshot](https://i.vgy.me/YAepNu.png)

![screenshot](https://i.vgy.me/WnfAkS.png)

## Passing the controlled devices list

### Autodiscovery

Opening the app via IPv4 URL starts autodiscovery in the /24 subnet, using port number set in `config.js` (before build, default is `9090`):

```
http://172.20.12.100:8000/ -> tries to find devices on 172.20.12.1:9090 - 172.20.12.255:9090
```

### Via URL parameter

It's also possible to set devices explicitly, separated by `,`:

```
http://localhost:3000/?172.20.12.197:9090,172.20.12.197:9091
```

(this disables autodiscovery)

## Building

```
$ npm install
$ npm run build
```

Built webapp is placed into `build/` directory and can be served by any webserver, eg.:

```
$ cd build
$ python3 -m http.server
```

If you intend to place it in a subdirectory (eg. `http://localhost:8000/ltemetr/`), set the `PUBLIC_URL` environment variable before building, so the app knows where to find its files:

```
$ export PUBLIC_URL="/ltemetr"
$ npm run build
```

## GitLab CI

The app is automatically built after a tag is pushed to GitLab, with `PUBLIC_URL` set to `/ltemetr`, and Sentry logging enabled.

The build can be downloaded via this URL:

```
https://gitlab.labs.nic.cz/jhelebrant/ltemetr-web/-/jobs/artifacts/VERSION/download?job=pages
```

(where `VERSION` is the tag name of course, eg. `v0.2`)

## Development

The app was started with [create-react-app](https://github.com/facebook/create-react-app), so all the usual stuff works – `npm start` runs a development server with autoreload & linter.
