export default {
  updateInterval: 1000,
  chartBarCount: 40,
  chartMaxSpeed: 200, // Mbps
  autodiscoveryPort: 9090,
  measurementTypes: ["TRAIN", "CAR"]
};
