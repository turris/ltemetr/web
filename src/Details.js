import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import colors from "./colors";

const Container = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 0.2em;
  padding: 0.5em;
  margin-left: 0.5em;
  position: absolute;
  top: 1em;
  right: 1em;
  font-family: monospace;
  color: ${props => (props.isRunning ? colors.white : colors.gray)};

  p {
    margin: 0 auto;
    white-space: nowrap;
  }
`;

const Label = styled.span`
    display: inline-block;
    min-width: 5em;
    margin-right: 0.5em;
    text-align: right;
`;

const Value = styled.span`
    font-weight: bold;
`;

Container.propTypes = {
  isRunning: PropTypes.bool
};

const getValue = (data, key) => data[key] ? data[key].toString() : ""

const Text = ({ data }) => (
    <React.Fragment>
         <p><Label>RSRP: </Label><Value>{getValue(data, "RSRP")}</Value></p>
         <p><Label>SINR: </Label><Value>{getValue(data, "SINR Rx[0]")}</Value></p>
         <p><Label>LAC: </Label><Value>{getValue(data, "LAC")}</Value></p>
         <p><Label>PCI: </Label><Value>{getValue(data, "PCI")}</Value></p>
         <p><Label>Cell ID: </Label><Value>{getValue(data, "Cell Id")}</Value></p>
         <p><Label>Bandwidth: </Label><Value>{getValue(data, "Bandwidth")}</Value></p>
         <p><Label>GPS lat: </Label><Value>{getValue(data, "lat").slice(0, 12)}</Value></p>
         <p><Label>GPS lon: </Label><Value>{getValue(data, "lon").slice(0, 12)}</Value></p>
    </React.Fragment>
);

const Details = ({ speeds, isRunning }) => (
    speeds && speeds.length > 0 ?(
        <Container> 
            <Text data={speeds[speeds.length - 1]} />
        </Container>
    ) : null
);

Details.propTypes = {
  data: PropTypes.object
};

export default React.memo(Details);
