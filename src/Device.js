import React from "react";
import styled from "styled-components";
import * as Sentry from "@sentry/browser";
import PropTypes from "prop-types";
import Chart from "./Chart";
import Details from "./Details";
import colors from "./colors";
import config from "./config";

const DeviceContainer = styled.div`
  font-size: 1rem;
  background-color: ${colors.deviceBg};
  margin: 1em 1em 0 0;
  width: calc(50% - 3.5em);
  display: flex;
  padding: 0 1em 1em;
  border-radius: 0.1em;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  border-left: 0.5em solid
    ${props =>
      props.isRunning === null
        ? colors.gray
        : props.isRunning
        ? colors.green
        : colors.red};

  @media screen and (max-width: 50em) {
    width: 100%;
  }
`;

DeviceContainer.propTypes = {
  isRunning: PropTypes.bool
};

const ButtonContainer = styled.div`
  flex: 1;
`;

const Button = styled.button`
  padding: 0.5em;
  border-radius: 0.1em;
  border: 0;
  font-size: 1rem;
  cursor: pointer;
  background: ${colors.lightGray};
  margin-right: 0.5em;
  text-align: center;
  min-width: 4em;
  color: black;

  &:disabled {
    background: ${colors.gray};
    color: ${colors.lightGray};
    cursor: not-allowed;
  }
`;

const StartButton = styled(Button)`
  background-color: ${colors.green};
`;

const StopButton = styled(Button)`
  background-color: ${colors.red};
`;

const Error = styled.div`
  background-color: ${colors.red};
  color: ${colors.white};
  padding: 0.25em;
  border-radius: 0.1em;
  margin: 0.5em 0;
`;

const ConfigForm = styled.form`
  label {
    display: flex;
    padding: 0.25em 0;
    white-space: nowrap;

    span {
      width: 50%;
    }

    input,
    select {
      flex: 1;
    }
  }
`;

const sendCommand = async (url, command) => {
  let r = await fetch(url + command, { method: "POST" });
  let j = await r.json();
  return j.running;
};

class Device extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      running: null,
      speeds: [],
      configShown: false,
      errors: [],
      message: null,
      settings: null,
      operator: null
    };
    this.storeSetting = this.storeSetting.bind(this);
    this.submitSettings = this.submitSettings.bind(this);
    this._isMounted = false;
  }

  static propTypes = {
    config: PropTypes.shape({
      url: PropTypes.string.isRequired
    }),
    updateInterval: PropTypes.number.isRequired
  };

  updateDeviceStatus() {
    fetch(this.props.config.url + "status")
      .then(r => r.json())
      .then(j => {
        if (j.success) {
          const errors = [...new Set(j.errors)];
          this._isMounted &&
            this.setState({ running: j.running, operator: j.operator, errors });
          errors.forEach(e =>
            Sentry.captureMessage(e, {
              tags: {
                device: this.props.config.url,
                level: "error",
                route: "status"
              }
            })
          );
          if (j.running) {
            this.startSpeedUpdateTimer();
          } else {
            clearInterval(this.speedUpdateTimer);
            this.speedUpdateTimer = null;
          }
        } else {
          this._isMounted && this.setState({ running: null });
        }
      })
      .catch(e => {
        this._isMounted && this.setState({ running: null });
        Sentry.captureMessage(`Device went offline: ${e}`, {
          tags: {
            device: this.props.config.url,
            level: "error",
            route: "status"
          }
        });
      });
  }

  updateSpeed() {
    fetch(this.props.config.url + "latest")
      .then(r => r.json())
      .then(j => {
        j.time &&
          this._isMounted &&
          this.setState(prevState => ({
            speeds: [...prevState.speeds, j].filter(
              (item, index, self) =>
                index === self.findIndex(t => t.time === item.time)
            )
          }));
      })
      .catch(e => {
        this._isMounted && this.setState({ running: null });
        Sentry.captureMessage(e, {
          tags: {
            device: this.props.config.url,
            level: "error",
            route: "latest"
          }
        });
      });
  }

  startSpeedUpdateTimer() {
    if (!this.speedUpdateTimer) {
      this.speedUpdateTimer = setInterval(
        this.updateSpeed.bind(this),
        Math.min(this.props.updateInterval, 1000)
      );
    }
  }

  async sendStart() {
    this._isMounted && this.setState({ running: true });
    const running = await sendCommand(this.props.config.url, "start");
    this._isMounted && this.setState({ running });
    this.startSpeedUpdateTimer();
  }

  async sendStop() {
    this._isMounted && this.setState({ running: false });
    const running = await sendCommand(this.props.config.url, "stop");
    this._isMounted && this.setState({ running });
    clearInterval(this.speedUpdateTimer);
    this.speedUpdateTimer = null;
  }

  showConfig() {
    fetch(this.props.config.url + "settings")
      .then(r => r.json())
      .then(j => {
        this._isMounted &&
          this.setState({
            settings: j,
            configShown: true
          });
      });
  }

  hideConfig() {
    this._isMounted && this.setState({ configShown: false });
  }

  componentDidMount() {
    this._isMounted = true;
    this.updateDeviceStatus();
    this.statusUpdateTimer = setInterval(
      this.updateDeviceStatus.bind(this),
      this.props.updateInterval
    );
  }

  componentWillUnmount() {
    this._isMounted = false;
    clearInterval(this.statusUpdateTimer);
    clearInterval(this.speedUpdateTimer);
  }

  storeSetting(event) {
    const name = event.target.name;
    const value = event.target.value;
    this._isMounted &&
      this.setState(prevState => ({
        settings: {
          ...prevState.settings,
          [name]: value
        }
      }));
  }

  submitSettings(event) {
    event.preventDefault();
    fetch(this.props.config.url + "settings", {
      method: "POST",
      body: JSON.stringify(this.state.settings)
    })
      .then(r => r.json())
      .then(j => {
        this._isMounted &&
          this.setState({
            settings: j.settings,
            message: j.success ? "Settings saved" : `Error: ${j.error}`,
            configShown: !j.success
          });
        if (j.error) {
          Sentry.captureMessage(`Error when saving settings: ${j.error}`, {
            tags: {
              device: this.props.config.url,
              level: "error",
              route: "settings"
            }
          });
        }
        setTimeout(
          () => this._isMounted && this.setState({ message: null }),
          5000
        );
      });
  }

  render() {
    return (
      <DeviceContainer isRunning={this.state.running}>
        <h2>
          {this.state.operator}{" "}
          {this.props.config.url.replace(/\//g, "").replace("http:", "")}
        </h2>
        <ButtonContainer>
          {this.state.running === true ? (
            <StopButton onClick={() => this.sendStop()}>Stop</StopButton>
          ) : null}
          {this.state.running === false ? (
            <StartButton
              onClick={() => this.sendStart()}
              disabled={this.state.configShown}
            >
              Start
            </StartButton>
          ) : null}
          {this.state.running !== null ? (
            <a href={this.props.config.url + "export"}>
              <Button>Export</Button>
            </a>
          ) : null}
          {this.state.running !== null ? (
            <Button
              onClick={() =>
                this.state.configShown ? this.hideConfig() : this.showConfig()
              }
              disabled={this.state.running}
            >
              Settings
            </Button>
          ) : null}
        </ButtonContainer>
        <>
          {this.state.errors.map(msg => (
            <Error key={msg}>{msg}</Error>
          ))}
        </>
        {this.state.configShown ? (
          <ConfigForm onSubmit={this.submitSettings}>
            <label>
              <span>Measurement type: </span>
              <select
                name="measurement_type"
                onChange={this.storeSetting}
                value={this.state.settings["measurement_type"]}
              >
                {config.measurementTypes.map(type => (
                  <option value={type} key={type}>
                    {type}
                  </option>
                ))}
              </select>
            </label>
            <label>
              <span>IPerf server: </span>
              <input
                type="text"
                name="iperf_server_address"
                value={this.state.settings["iperf_server_address"] || ""}
                onChange={this.storeSetting}
              />
            </label>
            <label>
              <span>Modem console: </span>
              <input
                type="text"
                name="modem_console_path"
                value={this.state.settings["modem_console_path"] || ""}
                onChange={this.storeSetting}
              />
            </label>
            <label>
              <span>GPS console: </span>
              <input
                type="text"
                name="gps_ctrl_console_path"
                value={this.state.settings["gps_ctrl_console_path"] || ""}
                onChange={this.storeSetting}
              />
            </label>
            {this.state.settings["measurement_type"] === "TRAIN" ? (
              <label>
                <span>Max. dist. to pause: </span>
                <input
                  type="text"
                  name="max_distance_to_pause"
                  pattern="[0-9]+"
                  value={this.state.settings["max_distance_to_pause"] || ""}
                  onChange={this.storeSetting}
                />
              </label>
            ) : null}
            <Button type="submit">Save</Button>
            {this.state.message ? <span>{this.state.message}</span> : null}
          </ConfigForm>
        ) : (
          <React.Fragment>
            <Chart isRunning={this.state.running} speeds={this.state.speeds} />
            <Details
              isRunning={this.state.running}
              speeds={this.state.speeds}
            />
          </React.Fragment>
        )}
      </DeviceContainer>
    );
  }
}

export default Device;
