import React from "react";
import ReactDOM from "react-dom";
import Chart from "./Chart";

const speeds = [
  { "IP Thrpt": 48774.76, time: "23.05.2019 14:13:26" },
  { "IP Thrpt": 48773.99, time: "23.05.2019 14:13:27" },
  { "IP Thrpt": 41431.22, time: "23.05.2019 14:13:28" },
  { "IP Thrpt": 31321.34, time: "23.05.2019 14:13:29" },
  { "IP Thrpt": 55333.66, time: "23.05.2019 14:13:30" }
];

it("renders without crashing when measurement is not running", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Chart isRunning={false} speeds={speeds} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders without crashing when measurement is running", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Chart isRunning={true} speeds={speeds} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
