import React from "react";
import styled from "styled-components";
import Device from "./Device";
import config from "./config";

const AppContainer = styled.div`
  background-color: #0e252d;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  font-size: calc(10px + 1.5vmin);
  color: white;
`;

const DeviceContainer = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  padding: 0 0 1em 1em;
`;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      devices: []
    };
  }

  componentDidMount() {
    if (window.location.search) {
      this.loadDevicesFromUrl();
    } else {
      this.loadDevicesFromAutodiscovery();
    }
  }

  loadDevicesFromUrl() {
    const devices = window.location.search
      .replace("?", "")
      .split(",")
      .map(host => ({
        url: `http://${host}/`
      }));
    this.setState({ devices });
  }

  loadDevicesFromAutodiscovery() {
    const hostname = window.location.hostname.replace(/:[0-9]*/, "");
    if (hostname.match(/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/)) {
      const potentialHosts = new Set();
      for (let i = 1; i < 256; i++) {
        const host = hostname.replace(
          /[0-9]{1,3}$/,
          `${i}:${config.autodiscoveryPort}`
        );
        potentialHosts.add(host);
      }
      potentialHosts.forEach(host =>
        fetch(`http://${host}/status`)
          .then(
            r =>
              r.ok &&
              this.setState(prevState => ({
                devices: [...prevState.devices, { url: `http://${host}/` }]
              }))
          )
          .catch(e => ({}))
      );
    }
  }

  render() {
    return (
      <AppContainer>
        <DeviceContainer>
          {this.state.devices.map(device => (
            <Device
              config={device}
              key={device.url}
              updateInterval={config.updateInterval}
            />
          ))}
        </DeviceContainer>
      </AppContainer>
    );
  }
}

export default App;
