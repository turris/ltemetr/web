import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import colors from "./colors";
import config from "./config";
import formatSpeed from "./formatSpeed";

const { chartMaxSpeed, chartBarCount } = config;
const styledPropTypes = {
  isRunning: PropTypes.bool
};

const ChartContainer = styled.svg`
  width: 100%;
  height: 100%;
  align-self: flex-end;
`;

const Rect = styled.rect`
  fill: ${props => (props.isRunning ? colors.lightGray : colors.gray)};
  stroke: none;
`;

Rect.propTypes = styledPropTypes;

const DashedLine = styled.line`
  stroke: ${props => (props.isRunning ? colors.lightGray : colors.gray)};
  stroke-width: 1px;
  stroke-dasharray: 4;
`;

DashedLine.propTypes = styledPropTypes;

const SpeedText = styled.text`
  font-size: 3em;
  font-weight: bold;
  stroke: ${colors.deviceBg};
  stroke-width: 0.2em;
  fill: ${props => (props.isRunning ? colors.white : colors.gray)};
  paint-order: stroke fill;
  text-align: center;
  text-anchor: middle;
  stroke-linejoin: round;
`;

Text.propTypes = styledPropTypes;

const PausedRect = styled.rect`
  fill: ${props => (props.isRunning ? colors.white : colors.gray)};
  stroke: ${colors.deviceBg};
  stroke-width: 0.2em;
`;


const Chart = ({ speeds, isRunning }) => (
  <ChartContainer>
    {[25, 50, 75].map(percentage => (
      <DashedLine
        isRunning={isRunning}
        key={percentage}
        x1="0"
        y1={`${percentage}%`}
        x2="100%"
        y2={`${percentage}%`}
      />
    ))}
    {speeds.length > 1
      ? speeds.slice(-1 * chartBarCount).map((speedInfo, i, speeds) => {
          const height = isNaN(speedInfo["IP Thrpt"])
            ? 0
            : (speedInfo["IP Thrpt"] / (chartMaxSpeed * 1000)) * 100;
          const width = 100 / chartBarCount;
          const key = `${speedInfo["IP Thrpt"]} ${speedInfo.time}`;
          const x = i * width + "%";
          return height > 0 ? (
            <React.Fragment key={key}>
              <Rect
                isRunning={isRunning}
                y={100 - height + "%"}
                width={width + "%"}
                x={x}
                height={height + "%"}
              />
              {i === speeds.length - 1 ? (
                <React.Fragment>
                  <SpeedText x="50%" y="95%" isRunning={isRunning}>
                    {formatSpeed(speedInfo["IP Thrpt"])}
                  </SpeedText>
                  {speedInfo["paused"] ? (
                    <React.Fragment>
                      <PausedRect isRunning={isRunning} x="0" y="20" width="30" height="80" />
                      <PausedRect isRunning={isRunning} x="40" y="20" width="30" height="80" />
                    </React.Fragment>
                  ) : null}
                </React.Fragment>
              ) : null}
            </React.Fragment>
          ) : null;
        })
      : null}
  </ChartContainer>
);

Chart.propTypes = {
  isRunning: PropTypes.bool,
  speeds: PropTypes.arrayOf(
    PropTypes.shape({
      time: PropTypes.string,
      "IP Thrpt": PropTypes.number
    })
  )
};

export default React.memo(Chart);
