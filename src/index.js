import React from "react";
import ReactDOM from "react-dom";
import * as Sentry from "@sentry/browser";
import { version } from '../package.json';
import "./index.css";
import App from "./App";

if (process.env.REACT_APP_SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    environment: process.env.NODE_ENV,
    release: version,
  });
}

ReactDOM.render(<App />, document.getElementById("root"));
document.title = "LTEmetr";
