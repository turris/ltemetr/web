export default {
  green: "#2effa4",
  red: "#ff312e",
  gray: "#537584",
  white: "#d9eef7",
  lightGray: "#759aab",
  deviceBg: "#143642"
};
