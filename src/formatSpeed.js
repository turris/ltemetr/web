const formatSpeed = speed => {
  let formatted, unit;
  if (speed === null || isNaN(speed)) {
    return "";
  }
  speed = speed / 1000;
  if (speed >= 1) {
    formatted = (Math.round(speed * 100) / 100).toString();
    unit = "Mb/s";
  } else {
    formatted = (Math.round(speed * 1000 * 100) / 100).toString();
    unit = "Kb/s";
  }
  if (formatted.length < 5) {
    formatted = formatted.replace(/([,.][0-9]+)$/, "$10");
  }
  return `${formatted} ${unit}`;
};

export default formatSpeed;
